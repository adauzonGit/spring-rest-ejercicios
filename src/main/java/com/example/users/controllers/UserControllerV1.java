package com.example.users.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.users.models.User;
import com.example.users.services.UserServiceV1;

@RestController
@RequestMapping("/v1/users")
public class UserControllerV1 {

	@Autowired
	private UserServiceV1 userService;

	@GetMapping
	public ResponseEntity<List<User>> getUsers() {
		return ResponseEntity.ok(userService.getUsers());
	}

	@GetMapping(value = "/{userName}")
	public ResponseEntity<User> getUserByUserName(@PathVariable("userName") String userName) {
		return ResponseEntity.ok(userService.getUserByUserName(userName));
	}

	@PostMapping
	public ResponseEntity<User> createUser(@RequestBody User user) {
		return ResponseEntity.status(HttpStatus.CREATED).body(userService.createUser(user));
	}

	@PutMapping("/{userName}")
	public ResponseEntity<User> updateUser(@RequestBody User user, @PathVariable String userName) {
		return ResponseEntity.status(HttpStatus.OK).body(userService.updateUser(user, userName));
	}
	@DeleteMapping("/{userName}")
	public ResponseEntity<Void> deleteUser(@PathVariable String userName){
		userService.deleteUser(userName);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		
	}
}
