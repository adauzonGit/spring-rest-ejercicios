package com.example.users.controllers;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.users.entity.Users;
import com.example.users.models.User;
import com.example.users.services.UserService;

import io.micrometer.core.annotation.Timed;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;

	@GetMapping
	@Timed(value = "getUsers")
	public ResponseEntity<Page<Users>> getUsers(
			@RequestParam(value = "page", required = false, defaultValue = "0") Integer length,
			@RequestParam(value = "size", required = false, defaultValue = "20") Integer size) {
		return ResponseEntity.ok(userService.getUsers(length, size));
	}

	@GetMapping("/{userId}")
	@ApiOperation(value = "Return a user for a given userId",response = User.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Succefull ")
	})
	public ResponseEntity<Users> getUserById(@PathVariable("userId") Integer userId) {

		return ResponseEntity.ok(userService.getUserById(userId));
	}

	@GetMapping("/username/{userName}")
	public ResponseEntity<Users> getUserByUserName(@PathVariable("userName") String username) {
		return ResponseEntity.ok(userService.getUserByUserName(username));
	}
	
	@DeleteMapping("/{userId}")
	public ResponseEntity<Void> deleteUserById(@PathVariable("userId") Integer userId) {
		userService.deleteUserById(userId);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}
}
