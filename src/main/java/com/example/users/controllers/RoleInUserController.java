package com.example.users.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.users.entity.UserInRole;
import com.example.users.services.RoleInUsersService;

@RestController
@RequestMapping("/roleinuser")
public class RoleInUserController {

	@Autowired
	private RoleInUsersService roleInUserService;
	
	@GetMapping
	public ResponseEntity<List<UserInRole>> getUserInRoles(){			
		return ResponseEntity.ok(roleInUserService.getUserInRoles());
	}
}
