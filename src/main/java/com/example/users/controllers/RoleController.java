package com.example.users.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.users.entity.Role;
import com.example.users.entity.Users;
import com.example.users.services.RoleService;

@RestController
@RequestMapping("/roles")
public class RoleController {

	@Autowired
	private RoleService roleService;

	
	@GetMapping("/{roleName}/users")
	public ResponseEntity<List<Users>> getUserByRole(@PathVariable("roleName") String roleName) {
		return ResponseEntity.ok(roleService.getUsersByRoleName(roleName));
	}
	
	@GetMapping
	public ResponseEntity<List<Role>> getRoles() {
		return ResponseEntity.ok(roleService.getRoles());
	}

	@PostMapping
	public ResponseEntity<Role> addRole(@RequestBody Role role) {
		return ResponseEntity.ok(roleService.createRole(role));
	}

	@PutMapping("/{roleId}")
	public ResponseEntity<Role> updateRole(@PathVariable("roleId") int roleID, @RequestBody Role role) {
		return ResponseEntity.ok(roleService.updateRole(roleID, role));
	}

	@DeleteMapping("/{roleId}")
	public ResponseEntity<Void> deleteRole(@PathVariable("roleId") Integer roleId) {
		roleService.deleteRole(roleId, null);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}
}
