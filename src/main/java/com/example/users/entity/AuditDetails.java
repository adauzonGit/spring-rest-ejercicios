package com.example.users.entity;

public class AuditDetails {

	private String createdBy;
	private String roleName;

	public AuditDetails() {
		// TODO Auto-generated constructor stub
	}

	public AuditDetails(String createdBy, String roleName) {

		this.createdBy = createdBy;
		this.roleName = roleName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}
