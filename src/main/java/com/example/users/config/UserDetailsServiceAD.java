package com.example.users.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.users.entity.UserInRole;
import com.example.users.repositorues.UserInRoleRepository;
import com.example.users.repositorues.UserRepository;

@Service
public class UserDetailsServiceAD implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserInRoleRepository userInRoleRepository;

	@Autowired
	private PasswordEncoder encoder;
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub

		var oUser = userRepository.findOneByUserName(username);
		if (oUser.isPresent()) {
			var user = oUser.get();
			List<UserInRole> userInRoles = userInRoleRepository.findByUser(user);
			String[] roles = userInRoles.stream().map(ur -> ur.getRole().getName()).toArray(String[]::new);
			return User.withUsername(user.getUserName()).password(encoder.encode(user.getPassword())).roles(roles).build();
		} else {
			throw new UsernameNotFoundException("Usernaeme" + username + "not found");
		}
		
	}

}
