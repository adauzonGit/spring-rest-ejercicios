package com.example.users;

import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.users.controllers.RoleController;
import com.example.users.entity.Role;
import com.example.users.entity.UserInRole;
import com.example.users.entity.Users;
import com.example.users.repositorues.RoleRepository;
import com.example.users.repositorues.UserInRoleRepository;
import com.example.users.repositorues.UserRepository;
import com.github.javafaker.Faker;

@SpringBootApplication
public class UsersAppApplication implements ApplicationRunner {

	@Autowired
	private Faker faker;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;
	
	
	private static final Logger log = LoggerFactory.getLogger(UsersAppApplication.class);

	@Autowired
	private UserInRoleRepository userInRoleRepository;

	public static void main(String[] args) {
		SpringApplication.run(UsersAppApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		// TODO Auto-generated method stub
		var roles = List.of(new Role("ADMIN"), new Role("USER"), new Role("SUPPORT"));
		roles.forEach(rol -> roleRepository.save(rol));

		for (int i = 0; i < 10; i++) {
			var user = new Users();
			user.setUserName(faker.name().firstName());
			user.setPassword(faker.name().username());
			var userCreated = userRepository.save(user);
			var userinrole = new UserInRole(userCreated, roles.get(new Random().nextInt(3)));			
			userInRoleRepository.save(userinrole);
			log.info("Username {} have the role {} whith password {} ",userCreated.getUserName(), userinrole.getRole().getName(), userCreated.getPassword());
		}
	}
}
