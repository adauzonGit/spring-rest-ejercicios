package com.example.users.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.example.users.entity.Users;
import com.example.users.repositorues.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public Page<Users> getUsers(int page, int size) {
		return userRepository.findAll(PageRequest.of(page, size));
		
	}

	public Users getUserById(Integer id) {
		return userRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
				String.format("User with id %d not found", id)));
	}
	@Cacheable("users")
	public Users getUserByUserName(String userName) {
		System.out.println("here");
		return userRepository.findOneByUserName(userName).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
				String.format("User with username %s not found", userName)));
	}
	
	@CacheEvict("users")
	public void deleteUserById(Integer id) {
		System.out.println("here");
		var user = getUserById(id);
		userRepository.delete(user);
	}
	
	public Users getUserByUserNameAndPassword(String username,String password) {
		return userRepository.findByUserNameAndPassword(username, password).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
				String.format("User with username %s not found", username)));
	}
}
