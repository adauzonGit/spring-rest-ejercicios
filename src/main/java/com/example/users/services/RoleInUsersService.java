package com.example.users.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.users.entity.UserInRole;
import com.example.users.repositorues.UserInRoleRepository;

@Service
public class RoleInUsersService {

	@Autowired
	private UserInRoleRepository userInRoleRepository;
	
	public List<UserInRole> getUserInRoles(){
		return userInRoleRepository.findAll();
	}
}
