package com.example.users.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.example.users.entity.AuditDetails;
import com.example.users.entity.Role;
import com.example.users.entity.Users;
import com.example.users.repositorues.RoleRepository;
import com.example.users.repositorues.UserInRoleRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class RoleService {

	@Autowired
	private RoleRepository roleRepository;

	private static final Logger log = LoggerFactory.getLogger(RoleService.class);

	@Autowired
	private UserInRoleRepository userInRoleRepository;

	@Autowired
	private KafkaTemplate<Integer, String> kafkaTemplete;

	public List<Role> getRoles() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		log.info("Name  {}", authentication.getName());
		log.info("Principal {}", authentication.getPrincipal());
		log.info("Credentials {}", authentication.getCredentials());
		log.info("Roles {}", authentication.getAuthorities().toString());
		return roleRepository.findAll();
	}

	@Secured({ "ROLE_ADMIN" })
	public List<Users> getUsersByRoleName(String roleName) {

		return userInRoleRepository.findUsersByRoleName(roleName);
	}

	public Role createRole(Role role) {

		Role rol = roleRepository.save(role);

		AuditDetails details = new AuditDetails();
		details.setRoleName(rol.getName());
		details.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
		try {
			kafkaTemplete.send("hola-que-hace", new ObjectMapper().writeValueAsString(details));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error parsing the message");
		}

		return rol;
	}

	public Role updateRole(int roleId, Role role) {

		var roleO = roleRepository.findById(roleId);
		if (roleO.isPresent()) {
			return roleRepository.save(role);
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No existe");
		}
	}

	public void deleteRole(int roleId, Role role) {
		var roleO = roleRepository.findById(roleId);

		if (roleO.isPresent()) {
			roleRepository.delete(roleO.get());
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No existe");
		}
	}
}
