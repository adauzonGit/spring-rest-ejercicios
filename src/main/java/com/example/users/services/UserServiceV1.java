package com.example.users.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.example.users.models.User;
import com.github.javafaker.Faker;

@Service
public class UserServiceV1 {

	@Autowired
	private Faker faker;

	private List<User> users = new ArrayList();
    private Set<User> usersw = new HashSet<>();
    private Map<String,Object> obj;
    private Runnable run;
    private Thread t;
	@PostConstruct
	public void init() {
		for (int i = 0; i < 100; i++) {
			users.add(
					new User(faker.dragonBall().character(), faker.funnyName().name(), faker.dragonBall().character()));
		}
	}

	public List<User> getUsers() {
		return users;
	}

	public User getUserByUserName(String userName) {
		"".hashCode();
		return users.stream().filter(user -> user.getUsername().equals(userName)).findFirst()
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User %s not found"));
	}

	public User createUser(User user) {
		if (users.stream().anyMatch(u -> u.getUsername().equals(user.getUsername()))) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "User exists");
		}

		users.add(user);
		return user;
	}

	public User updateUser(User user, String userName) {
		User userToUpdate = getUserByUserName(userName);
		userToUpdate.setNickname(user.getNickname());
		userToUpdate.setPassword(user.getPassword());
		
		return userToUpdate;
	}
	
	public void deleteUser(String userName) {
		User userToDelete = getUserByUserName(userName);
		users.remove(userToDelete);
	}
}
