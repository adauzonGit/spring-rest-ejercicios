package com.example.users.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaListenerEvent {

	private static final Logger log = LoggerFactory.getLogger(KafkaListenerEvent.class);

	@KafkaListener(topics = "hola-que-hace", groupId = "adauzonGroup")
	public void listen(String message) {
		log.info("The message is {} ", message);
	}

}
