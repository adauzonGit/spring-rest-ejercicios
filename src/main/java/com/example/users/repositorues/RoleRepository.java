package com.example.users.repositorues;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.users.entity.Role;

@Repository
public interface RoleRepository  extends JpaRepository<Role, Integer>{

}
