package com.example.users.repositorues;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.users.entity.Address;

@Repository
public interface AddressRepository extends CrudRepository<Address, Integer> {

}
