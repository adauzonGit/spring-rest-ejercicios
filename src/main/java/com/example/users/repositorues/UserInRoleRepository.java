package com.example.users.repositorues;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.users.entity.UserInRole;
import com.example.users.entity.Users;

public interface UserInRoleRepository extends JpaRepository<UserInRole, Integer> {

	@Query("select u.user from UserInRole u where u.role.name = ?1")
	public List<Users> findUsersByRoleName(String roleName);
	
	
	public List<UserInRole> findByUser(Users user);
}
