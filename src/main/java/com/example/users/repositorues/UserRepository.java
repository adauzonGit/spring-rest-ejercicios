package com.example.users.repositorues;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.users.entity.Users;

@Repository
public interface UserRepository extends JpaRepository<Users, Integer>{

	
	public Optional<Users> findOneByUserName(String username);
	public Optional<Users> findByUserNameAndPassword(String username, String password);
}
