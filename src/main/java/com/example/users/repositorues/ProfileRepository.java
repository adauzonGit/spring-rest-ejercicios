package com.example.users.repositorues;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.users.entity.Profile;

@Repository
public interface ProfileRepository extends CrudRepository<Profile, Integer>{

}
